#######
Plugins
#######

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Available monitoring plugins

   check_*

.. toctree::
   :hidden:

   json
