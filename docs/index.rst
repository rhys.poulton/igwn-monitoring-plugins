.. toctree::
    :hidden:

    IGWN Monitoring Plugins <self>

#######################
IGWN Monitoring Plugins
#######################

The `igwn-monitoring-plugins` project provides a number of monitoring plugins
and an associated Python library for use by the International
Gravitational-Wave Observatory Network (IGWN).

=======
Plugins
=======

For details of the plugins provided by this project, see
:doc:`plugins/index`.

.. toctree::
    :hidden:

    plugins/index

==========
Python API
==========

For details of the Python API provided by this project, see
:doc:`api/index`.

.. toctree::
    :hidden:

    api/index
