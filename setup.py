# -*- coding: utf-8 -*-
# Copyright (C) Cardiff University (2023)
# SPDX-License-Identifier: MIT

"""Setup the igwn-monitor Python package
"""

from setuptools import setup
setup(use_scm_version=True)
